export interface User{
    id?:number,
    username:string,
    password:string,
    image?:string
}

export interface Food{
    id:number,
    user_id:number,
    food:string,
    createRecord:Date,
    
}