import { Knex } from "knex";
//import { Request, Response } from "express";
// import { Request } from "node-fetch";
// No request, response related (not depends on express)
// const getTime = require('./test.js');

// Less coupling
export class FoodService {
  constructor(private knex: Knex) {}

  async createFood(user_id: number, food: string, createRecord: Date) {
    await this.knex
      .insert({
        user_id: user_id,
        food: food,
        createRecord: createRecord,
      })
      .into("history");
  }

  async showFood(user_id: any) {
    //const user_id = req.session['user'];

    let builder = await this.knex
      .select("*")
      .from("foodnutrition")
      .innerJoin("history", "foodnutrition.food", "history.food")
      .where("history.user_id", "=", user_id)
      .orderBy("history.createRecord", "desc");
    // if(searchString){
    //     builder = builder.where('content','like','%'+searchString+'%');
    // }
    console.log(builder);

    return builder;
  }

  async showDate(user_id: any) {

    var myCurrentDate=new Date();
    var myPastDate=new Date(myCurrentDate);
        myPastDate.setDate(myPastDate.getDate() - 30);
    const past = myPastDate.toISOString();
    var thisDate=new Date(myCurrentDate);
        thisDate.setDate(thisDate.getDate());
    const now = thisDate.toISOString();


    let date = await this.knex
      .select("*")
      .from("foodnutrition")
      .innerJoin("history", "foodnutrition.food", "history.food")
      .where("history.user_id", "=", user_id)
      .whereBetween("history.createRecord", [past, now]);

    console.log(date);

    return date;
  }

  // async updateFood(id:number,content:string){
  //     await this.knex('food').update({content:content}).where({id:id});
  // }

  // async deleteFood(id:number){
  //     await this.knex('food').where('id',id).del();
  // }
}
