// //database related code 
import {Knex} from "knex";
import {User} from "./models";
import { hashPassword } from "../hash";
export class UserService {
    constructor(private knex: Knex) {}

    async getUsers(username: string) {
        const user = await this.knex<User>('users').where('email',username).first();
        return user;
    }

    async createUser(username:string, password:string, email:string, gender:string, date_of_birth:string, phone_number:string){
        const newAccount = await this.knex.insert({
            username:username,
            password:await hashPassword(password),
            email: email,
            gender:gender,
            date_of_birth:date_of_birth,
            phone_number:phone_number
        }).into('users')

        return newAccount
    }
}
