

            Number.prototype.padLeft = function(base,chr){
                var  len = (String(base || 10).length - String(this).length)+1;
                return len > 0? new Array(len).join(chr || '0')+this : this;
                
            }
            let d = new Date,
            dformat = [d.getFullYear().padLeft(),
                       d.setMonth(d.getMonth()).padLeft(),
                       d.getDate()].join('-') +' ' +
                      [d.getHours().padLeft(),
                       d.getMinutes().padLeft(),
                       d.getSeconds().padLeft()].join(':');

                       let y = new Date,
                       yformat = [y.getFullYear().padLeft(),
                                  y.setMonth(y.getMonth()-1).padLeft(),
                                  y.getDate()].join('-') +' ' +
                                 [y.getHours().padLeft(),
                                  y.getMinutes().padLeft(),
                                  y.getSeconds().padLeft()].join(':');