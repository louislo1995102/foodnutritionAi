import bcrypt from 'bcryptjs';

const saltRound = 10;

export async function hashPassword(plainPassword:string) {
    return await bcrypt.hash(plainPassword,saltRound);
}

export async function checkPassword(plainPassword:string, hashPassword:string) {
    return await bcrypt.compare(plainPassword, hashPassword)
}
