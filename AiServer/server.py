# %%
import tensorflow as tf
import numpy as np
from tensorflow import keras
from keras.models import load_model
from PIL import Image
from sanic import Sanic
from sanic.response import json
from keras.preprocessing import image
import os

print("TensorFlow version: {}".format(tf.__version__))
print("Eager execution: {}".format(tf.executing_eagerly()))

app = Sanic("You Are What You Eat")

#C:\\Users\\Louis\\food\\AiServer\\
#model = keras.models.load_model('model.h5')
# model = keras.models.load_model('C:\\Users\\Louis\\food\\AiServer\\model.h5')
#C:\\Users\\Louis Lo\\project\\food\\food\\food\\AiServer\\model.h5
#'/Users/johnpang1020/YouAreWhatYouEat/food/AiServer/model.h5'
model = keras.models.load_model('model.h5')


class_names =['SatayBeefNoodle','SingaporeStir-friedNoodles','Stir-friedBeefNoodle','fuknirice']


@app.get("/prediction")
async def pre(request):
    print(request.args['name'][0])
    a = request.args['name'][0]

    # a = "C:\\Users\\Louis\\food\\AiServer\\" + a
    a = os.path.join(os.path.dirname(__file__) , a)

    b = tf.keras.preprocessing.image.load_img(
        a, grayscale=False, color_mode="rgb", target_size=(224,224)
    )

    input_arr = (tf.keras.preprocessing.image.img_to_array(b) / 127.5) - 1
    input_arr = np.array([input_arr])  # Convert single image to a batch.
    predictions = model.predict(input_arr)
    print(predictions)
    FoodName = ''
    guest = np.argmax(predictions)
    if guest == 2:
        FoodName = 'Stir-fried-BeefNoodle'
    elif guest == 1:
        FoodName = 'SingaporeStir-friedNoodles'
    elif guest == 3:
        FoodName = 'fuknirice'
    else :
        FoodName = 'SatayBeefNoodle'
    
    
   
    return json({'food': FoodName} )

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)

