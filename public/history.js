document.querySelector('.home').addEventListener('click',function(){
  window.location.replace("./index.html");
})

document.querySelector('.history').addEventListener('click',function(){
  window.location.replace("./history.html");
})

document.querySelector('#bottomCenterButton').addEventListener('click',function(){
  window.location.replace("./webcam.html");
})

document.querySelector('.diagnosis').addEventListener('click',function(){
  window.location.replace("./diagnosis.html");
})


async function loadFoods() {

  const res = await fetch('/findFood'); // DEFAULT 係Get Method
  const foods = await res.json();
  console.log(foods)

  const historyTable = document.querySelector('#content');
  // clientInfoTable.innerHTML = '';

  for (let food of foods) {


      console.log(food)
      historyTable.innerHTML += `
      <div class="dietRecord ${food.food}">
            <div class="dietRecordHeader">
              <div class="col-md-3"></div>
              <div class="col-md-6">
                <div class="recordHeader"><div class="foodHeader">${food.food}</div>
                <div class="foodPhoto"><img src="./pictures/${food.food}.jpg" style="height:10vw;width:10vw;"></div></div>
              </div>
              <div class="col-md-3">
                <div class="recordDate">${moment(`${food.createRecord}`).format("YYYY-MM-DD HH:mm")}</div>
              </div>
            </div>

            <div class="nutritionObtained">
              <div class="nutritionObtainedHeader">Nutrition Obtained</div>
              <div class="nutritionObtainedContent ${food.food}${food.id}nu">
              </div>
            </div>
            <div class="nutritionDeficiency">
              <div class="nutritionDeficiencyHeader">Nutrition Deficiency</div>
              <div class="nutritionDeficiencyContent ${food.food}${food.id}de">
              </div>
            </div>
          </div>`

          let nutritions=['Protein','Biotin','Folate','VitaminA','VitaminB1','VitaminB2','VitaminB3','VitaminB6','VitaminB12'
        ,'VitaminC','VitaminD','VitaminE','VitaminK','Boron','Calcium','Chlorine','Chromium','Copper','Fluorine',
        'Iodine','Iron','Magnesium','Manganese','Molybdenum','Nickel','Phosphorus','Potassium','Selenium','Sodium','Vanadium','Zinc'];
          for(let nutrition of nutritions){
            if(food[nutrition]===true){
              document.querySelector(`.${food.food}${food.id}nu`).innerHTML+=`<div class="col-md-4">${nutrition}</div>`
            } else if(food[nutrition]===false){
              document.querySelector(`.${food.food}${food.id}de`).innerHTML+=`<div class="col-md-4">${nutrition}</div>`
            }
          }


  }
}

//console.log("hihi");


window.onload = async (event) => {
  await loadFoods()
}

