

const camera = document.querySelector('#replace')

camera.addEventListener('click',(e)=>{
  window.location = 'http://localhost:8080/webcam.html'
})



document.querySelector('.home').addEventListener('click',function(){
  window.location.replace("./index.html");
})

document.querySelector('.history').addEventListener('click',function(){
  window.location.replace("./history.html");
})


document.querySelector('.diagnosis').addEventListener('click',function(){
  window.location.replace("./diagnosis.html");
})
let ProteinDe=['Edema','Fatty Liver','Skin, Hair and Nail Problems','Loss of Muscle Mass','Greater Risk of Bone Fractures','Stunted Growth in Children','Increased Severity of Infections','Greater Appetite and Calorie Intake'];
let ProteinRe=['lean meats','poultry','fish and seafood','dairy products'];
let BiotinRe=['organ meats', 'eggs', 'fish', 'meat', 'seeds', 'nuts','certain vegetables']
let BiotinDe=['red rashes on the skin, especially the face',
'dry or scaly skin',
'dry eyes',
'brittle hair',
'hair loss',
'fatigue',
'insomnia or difficulty sleeping',
'loss of appetite',
'nausea',
'depression',
'burning or prickling sensation in the hands and feet',
'muscle pain',
'changes in the intestinal tract (frequent upset stomach)',
'cracking in the corners of the mouth',
'seizures',
'difficulty walking'];
let FolateDe=[
  'Crohn’s disease',
'celiac disease',
'certain types of cancers',
'severe kidney problems that require dialysis'
];
let FolateRe=[
  'broccoli',
'brussels sprouts',
'leafy green vegetables, such as cabbage, kale, spring greens and spinach',
'peas',
'chickpeas and kidney beans',
'liver (but avoid this during pregnancy)',
'breakfast cereals fortified with folic acid'
]

let BoronDe=[
  'altered macromineral', 'electrolyte', 'energy substrate', 'nitrogen, and oxidative metabolism', 'changes to erythropoiesis','hematopoiesis','depressed psychomotor skills'
  , 'depressed cognitive processes of attention and memory','electroencephalogram'
]
let BoronRe=[
  'Prune Juice',
  'Raw Avocado',
  'Raisins'

]

let CalciumDe=[
  'reduce bone strength',
  'lead to osteoporosis'
]

let CalciumRe=[
  'milk', 'cheese' ,'other dairy foods','green leafy vegetables','soya drink'
]

let ChlorineDe=[
  'fluid loss',
  'dehydration',
  'weakness or fatigue',
  'difficulty breathing',
  'diarrhea or vomiting, caused by fluid loss',

]
let ChlorineRe=[
  'wheat', 'rye', 'oats', 'whole grain', 'cereal bread','cereals'
]
let ChromiumDe=[
  'impaired insulin function',
  'inhibition of protein synthesis and energy production', 'type 2 diabetes and cardiovascular dysfunctions'
]
let ChromiumRe=[
  'broccoli', 'green beans', 'potatoes', 'apples', 'bananas', 'whole grains', 'peas', 'cheese', 'corn', 'grapes', 'beef', 'poultry'
]
let CopperDe=[
  'connective tissue', 'muscle weakness', 'anemia', 'low white blood cell count', 'neurological problems', 'paleness'
]

let CopperRe=[
  'shellfish', 'seeds and nuts', 'organ meats', 'wheat-bran cereals', 'whole-grain products','chocolate'
]
let FluorineDe=[
  'increased dental caries',
  'osteoporosis'
]
let FluorineRe=[
  'Brewed Coffee',
  'Brewed black tea',
  'Cooked Oatmeal'
]
let IodineDe=[
  'heart disease and related disorders',
  'depression and cognitive impairment',
  'peripheral neuropathy'
]
let IodineRe=[
  'Fish (such as cod and tuna)', 'seaweed', 'shrimp'
]
let IronDe=[
  'Extreme fatigue',
  'Weakness',
'Pale skin',
'Chest pain, fast heartbeat or shortness of breath',
'Headache, dizziness or lightheadedness',
'Cold hands and feet',
'Inflammation or soreness of your tongue',
'Brittle nails',
'Unusual cravings for non-nutritive substances, such as ice, dirt or starch',
'Poor appetite, especially in infants and children with iron deficiency anemia',
]
let IronRe=[
  'liver (but avoid this during pregnancy)',
'red meat',
'beans, such as red kidney beans, edamame beans and chickpeas',
'nuts',
'dried fruit – such as dried apricots',
'fortified breakfast cereals',
'soy bean flour',
]
let MagnesiumDe=[
  'loss of appetite', 'nausea', 'vomiting, fatigue', 'weakness','numbness', 'tingling', 'muscle contractions and cramps', 'seizures', 'personality changes', 'abnormal heart rhythms', 'coronary spasms'
]
let MagnesiumRe=[
  'greens', 'nuts', 'seeds', 'dry beans', 'whole grains', 'wheat germ', 'wheat', 'oat bran'
]

let ManganeseDe=[
  'bone demineralization and poor growth in children', 'skin rashes', 'hair depigmentation', 'decreased serum cholesterol', 'increased alkaline phosphatase activity in men','altered mood and increased premenstrual pain in women'
]
let ManganeseRe=[
  'whole grains', 'clams', 'oysters', 'mussels', 'nuts', 'soybeans and other legumes', 'rice', 'leafy vegetables', 'coffee', 'tea', 'many spices, such as black pepper'
]
let MolybdenumDe=[
  'irritability', 'tachycardia', 'tachypnea', 'night blindness'
]
let MolybdenumRe=[
  'Legumes such as black-eyed peas and lima beans',
'Whole grains, rice, nuts, potatoes, bananas, and leafy vegetables',
'Dairy products, like milk, yogurt, and cheese',
'Beef, chicken, and eggs',
]
let NickelDe=[
  'allergy', 'cardiovascular and kidney diseases', 'lung fibrosis', 'lung and nasal cancer'
]
let NickelRe=[
  'black tea',
'nuts and seeds',
'soy milk and chocolate milk',
'chocolate and cocoa powders',
'certain canned and processed foods, including meat and fish (check labels)',
'certain grains, including: oats. buckwheat. whole wheat. wheat germ',
]
let PhosphorusDe=[
  'anorexia', 'anemia', 'proximal muscle weakness', 'skeletal effects (bone pain, rickets, and osteomalacia)', 'increased infection risk, paresthesias', 'ataxia', 'confusion'
]
let PhosphorusRe=[
  'milk and milk products', 'meat', 'beans', 'lentils' ,'nuts' , 'grains'
]

let PotassiumDe=[
  'high blood pressure', 'constipation', 'muscle weakness', 'fatigue'
]

let PotassiumRe=[
  'Dried fruits (raisins, apricots)',
'Beans, lentils',
'Potatoes',
'Winter squash (acorn, butternut)',
'Spinach, broccoli',
'Beet greens',
'Avocado',
'Bananas',
]

let SeleniumDe=[
  'infertility in men and women',
'muscle weakness',
'fatigue',
'mental fog',
'hair loss',
'weakened immune system',
]
let SeleniumRe=[
  'Pork', 'beef', 'turkey', 'chicken', 'fish', 'shellfish', 'eggs'
]
let SodiumDe=[
  'headaches', 'seizures', 'coma', 'and even death'
]
let SodiumRe=[
'Smoked, cured, salted or canned meat, fish or poultry',
'Frozen breaded meats and dinners, such as burritos and pizza',
'Canned entrees, such as ravioli, spam and chili',
'Salted nuts',
'Beans canned with salt added',
]
let VanadiumDe=[
  'growth retardation', 'bone deformities', 'and infertility'
]
let VanadiumRe=[
  'Skim milk', 'lobster', 'vegetable oils', 'many vegetables, grains and cereals'
]

let ZincDe=[
  'growth retardation', 'loss of appetite', 'impaired immune function'
]

let ZincRe=[
  'oysters','red meat','poultry','beans'
]
let VitaminADe=[
  'dry eyes', 'blindness', 'dying corneas'
]
let VitaminARe=[
  'cheese',
'eggs',
'oily fish',
'fortified low-fat spreads',
'milk and yoghurt',
]

let VitaminB1De=[
  'wet beriberi', 'dry beriberi'
]

let VitaminB1Re=[
  'Fortified breakfast cereals',
'Pork',
'Fish',
'Beans, lentils',
'Green peas',
'Enriched cereals, breads, noodles, rice',
'Sunflower seeds',
'Yogurt',
]

let VitaminB2De=[
  'skin disorders', 'hyperemia (excess blood) and edema of the mouth and throat',
   'angular stomatitis (lesions at the corners of the mouth)', 
   'cheilosis (swollen, cracked lips)', 'hair loss', 'reproductive problems', 'sore throat',
   'itchy and red eyes','degeneration of the liver and nervous system'
]
let VitaminB2Re=[
  'milk',
'eggs',
'fortified breakfast cereals',
'mushrooms',
'plain yoghurt',
]

let VitaminB3De=[
  'triad of dermatitis', 'dementia', 'diarrhea'
]
let VitaminB3Re=[
  'beef', 'liver', 'poultry', 'eggs', 'dairy products', 'fish', 'nuts', 'seeds', 'legumes', 'avocados', 'whole grains'
]
let VitaminB6De=[
  'peripheral neuropathy','a pellagra-like syndrome','depression','confusion','electroencephalogram abnormalities','seizures'
]
let VitaminB6Re=[
  'pork',
'poultry, such as chicken or turkey',
'some fish',
'peanuts',
'soya beans',
'wheatgerm',
'oats',
'bananas',
]
let VitaminB12De=[
  "Crohn's disease", "celiac disease", "bacterial growth",'a parasite'
]

let VitaminB12Re=[
  'meat',
'fish',
'milk',
'cheese',
'eggs',
'some fortified breakfast cereals'
]
let VitaminCDe=[
  'fatigue', 'depression', 'connective tissue defects (eg, gingivitis, petechiae, rash, internal bleeding, impaired wound healing)'
]
let VitaminCRe=[
  'citrus fruit, such as oranges and orange juice',
'peppers',
'strawberries',
'blackcurrants',
'broccoli',
'brussels sprouts',
'potatoes',
]
let VitaminDDe=[
  'having dark skin',
'being an older adult',
'being overweight or having obesity',
'not eating much fish or dairy',
'living far from the equator in areas where there is little sun year-round',
'always using sunscreen when going out; however, using sunscreen is important in helping prevent sun-damaging effects to the skin, including skin cancer',
'staying indoors',
'having chronic kidney disease, liver disease, or hyperparathyroidism',
'having a health condition that affects nutrient absorption, such as Crohn’s disease or celiac disease',
'having gastric bypass surgery',
'using certain medications that impact vitamin D metabolism'
]

let VitaminDRe=[
  'oily fish – such as salmon, sardines, herring and mackerel',
'red meat',
'liver',
'egg yolks',
'fortified foods – such as some fat spreads and breakfast cereals'
]
let VitaminEDe=[
  'impaired reflexes and coordination', 'difficulty walking', 'weak muscles'
]
let VitaminERe=[
  'Vegetable oils (such as wheat germ, sunflower, safflower, corn, and soybean oils)',
'Nuts (such as almonds, peanuts, and hazelnuts/filberts)',
'Seeds (such as sunflower seeds)',
'Green leafy vegetables (such as spinach and broccoli)'
]
let VitaminKDe=[
  'significant bleeding', 'poor bone development', 'osteoporosis', 'increased cardiovascular disease'
]
let VitaminKRe=[
  'Prunes — 24% DV per serving. 5 pieces: 28 mcg (24% DV)',
'Kiwi — 23% DV per serving',
'Avocado — 18% DV per serving',
]


let De=[];
De.push({"ProteinDe":ProteinDe},{"BiotinDe":BiotinDe},
{"FolateDe":FolateDe},{"VitaminADe":VitaminADe},
{"VitaminB1De":VitaminB1De},{"VitaminB2De":VitaminB2De},{"VitaminB3De":VitaminB3De},
{"VitaminB6De":VitaminB6De},{"VitaminB12De":VitaminB12De},
{"VitaminCDe":VitaminCDe},{"VitaminDDe":VitaminDDe},
{"VitaminEDe":VitaminEDe},{"VitaminKDe":VitaminKDe},
{"BoronDe":BoronDe},{"CalciumDe":CalciumDe},
{"ChlorineDe":ChlorineDe},{"ChromiumDe":ChromiumDe},
{"CopperDe":CopperDe},{"FluorineDe":FluorineDe},
{"IodineDe":IodineDe},{"IronDe":IronDe},
{"MagnesiumDe":MagnesiumDe},{"ManganeseDe":ManganeseDe},
{"MolybdenumDe":MolybdenumDe},{"NickelDe":NickelDe},
{"PhosphorusDe":PhosphorusDe},{"PotassiumDe":PotassiumDe},
{"SeleniumDe":SeleniumDe},{"SodiumDe":SodiumDe},
{"VanadiumDe":VanadiumDe},{"ZincDe":ZincDe});
let Re=[];
Re.push({"ProteinRe":ProteinRe},{"BiotinRe":BiotinRe},
{"FolateRe":FolateRe},{"VitaminARe":VitaminARe},
{"VitaminB1Re":VitaminB1Re},{"VitaminB2Re":VitaminB2Re},{"VitaminB3Re":VitaminB3Re},
{"VitaminB6Re":VitaminB6Re},{"VitaminB12Re":VitaminB12Re},
{"VitaminCRe":VitaminCRe},{"VitaminDRe":VitaminDRe},
{"VitaminERe":VitaminERe},{"VitaminKRe":VitaminKRe},
{"BoronRe":BoronRe},{"CalciumRe":CalciumRe},
{"ChlorineRe":ChlorineRe},{"ChromiumRe":ChromiumRe},
{"CopperRe":CopperRe},{"FluorineRe":FluorineRe},
{"IodineRe":IodineRe},{"IronRe":IronRe},
{"MagnesiumRe":MagnesiumRe},{"ManganeseRe":ManganeseRe},
{"MolybdenumRe":MolybdenumRe},{"NickelRe":NickelRe},
{"PhosphorusRe":PhosphorusRe},{"PotassiumRe":PotassiumRe},
{"SeleniumRe":SeleniumRe},{"SodiumRe":SodiumRe},
{"VanadiumRe":VanadiumRe},{"ZincRe":ZincRe});
let deficiency=[];
let recommendation=[];

async function checkMonthly() {

  const res = await fetch('/calDate'); // DEFAULT 係Get Method
  const recentRecords = await res.json();
  console.log(recentRecords)

  // clientInfoTable.innerHTML = '';
  let nutri=[];
  let nutritionDeficiency =[];
  let nutritions=['Protein','Biotin','Folate','VitaminA','VitaminB1','VitaminB2','VitaminB3','VitaminB6','VitaminB12'
  ,'VitaminC','VitaminD','VitaminE','VitaminK','Boron','Calcium','Chlorine','Chromium','Copper','Fluorine',
  'Iodine','Iron','Magnesium','Manganese','Molybdenum','Nickel','Phosphorus','Potassium','Selenium','Sodium','Vanadium','Zinc'];
  


  for (let recentRecord of recentRecords) {
        
          nutri.push(recentRecord['Folate']);
        
        
          for(let nutrition of nutritions){
            
            if(recentRecord[nutrition]===false){
              nutritionDeficiency.push(nutrition)
            }

          }

          


  }
  const counts = {};
  nutritionDeficiency.forEach(function (x) { counts[x] = (counts[x] || 0) + 1; });
  console.log(counts)

  for(let nutrition of nutritions){
    
    if(counts[nutrition]===nutri.length){
      document.querySelector('#content').innerHTML +=
    `<div id="${nutrition}" class="nutrition">
    <div class="nutritionTitle">LACK OF <div style="font-family: 'Special Elite', cursive;color:white;font-size:2.2vw;">${nutrition}</div> might HAVE THE RISKS below:</div>
    <div id="${nutrition}De" class="nutritionDe"></div>
    <div class="nutritionTitle">For <div style="font-family: 'Special Elite', cursive;color:white;font-size:2.2vw;">${nutrition}</div>, YOU CAN EAT THE FOOD BELOW</div>
    <div id="${nutrition}Re" class="nutritionRe"></div>
    </div>`
    let deficiencyPush = nutrition+'De';
    deficiency.push(deficiencyPush);
    let recommendationPush = nutrition+'Re';
    recommendation.push(recommendationPush);

    console.log(deficiency)
    console.log(deficiency[0])
    

    }


    // document.querySelector('#content').innerHTML +=
    // `<p>${counts[nutrition]}</p>`
  }
  for(let x=0;x<De.length;x++){
    for(let y=0;y<deficiency.length;y++){

      if(Object.keys(De[x])[0]===deficiency[y]){
        console.log(De[x][`${deficiency[y]}`]);
        let result= De[x][`${deficiency[y]}`];
        for(let i=0;i<result.length;i++){
          document.querySelector(`#${Object.keys(De[x])[0]}`).innerHTML+=`<div><li>${result[i]}</li></div>`;
        }
        
      }
  
    }
  }
  for(let j=0;j<Re.length;j++){
    for(let k=0;k<recommendation.length;k++){
      if(Object.keys(Re[j])[0]===recommendation[k]){
        console.log(Re[j][`${recommendation[k]}`]);
        let result1= Re[j][`${recommendation[k]}`];
        for(let l=0;l<result1.length;l++){
          document.querySelector(`#${Object.keys(Re[j])[0]}`).innerHTML+=`<div><li>${result1[l]}</li></div>`;
        }
      }
    }
  }

}

//   document.querySelector('#content').innerHTML +=
// `<h1>${nutri.length}</h1>`




//console.log("hihi");


window.onload = async (event) => {
  await checkMonthly()
}

