document.querySelector('#login-form').onsubmit = async function(event){
    event.preventDefault();

    const form = event.target
    const formOBj = {
        username: form.username.value,
        password: form.password.value
    }

    const res = await fetch('/login',{
        method:'POST',
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(formOBj)
    });

    const result = await res.json();
    console.log(res)
    console.log(result)
    if(res.status === 200){
        window.location = "/index.html"
    }
}
