document.querySelector('#do-registration').onsubmit = async function(event){
    event.preventDefault();

    const form = event.target
    const formObj = {
        username:form.username.value,
        password: form.password.value,
        phone_number:form.phone_number.value,
        email:form.email.value,
        gender:form.gender.value,
        date_of_birth:form.date_of_birth.value
    }
    
    const res = await fetch('/register',{
        method:'POST',
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(formObj)
    });

    const result = await res.json();
    if(res.status === 200){
        window.location('/index.html')
    }else{
        res.status(400).json({message:'failed to register'})
    }
}   
