import express from 'express';
import expressSession from 'express-session';
import Knex from 'knex'
import dotenv from 'dotenv'
//import fs from 'fs';
import {isLoggedIn} from './guard'
//import {User} from './services/models'
//import fetch from 'node-fetch'

import { FoodService } from './services/FoodService';
import { FoodController } from './controllers/FoodController';
// import exp from 'constants';
import { UserService } from './services/UserService';
import { UserController } from './controllers/UserController';


dotenv.config()
const app = express();
app.use(express.urlencoded({extended:true}));
app.use(express.json());
const knexConfigs = require('./knexfile');
const configMode = process.env.NODE_ENV || "development"
const knexConfig = knexConfigs[configMode]
const knex = Knex(knexConfig)

app.use(expressSession({
  secret:'food_project',
  resave:true,
  saveUninitialized:true
}))


export const foodService = new FoodService(knex);
export const foodController = new FoodController(foodService);
export const userService = new UserService(knex);
export const userController = new UserController(userService);
import {routes} from './Routes'
app.use('/',routes)


  
 

app.use(express.static("public"))
app.use(isLoggedIn,express.static("protected"))
app.use((req,res)=>{
  res.redirect('/404.html')
})
const PORT = 8080;

app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});


