
import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(!await knex.schema.hasTable("foodnull")){
        await knex.schema.createTable("foodnull",(table)=>{
            table.increments();
            table.string('food');
            table.boolean("Biotin");
            table.boolean("Folate");
            table.boolean("Vitamin A");
            table.boolean("Vitamin B1");
            table.boolean("Vitamin B2");
            table.boolean("Vitamin B3");
            table.boolean("Vitamin B6");
            table.boolean("Vitamin B12"); 
            table.boolean("Vitamin C");
            table.boolean("Vitamin D");
            table.boolean("Vitamin E");
            table.boolean("Vitamin K");
            table.boolean("Boron");
            table.boolean("Calcium");
            table.boolean("Chlorine");
            table.boolean("Chromium");
            table.boolean("Copper");
            table.boolean("Fluorine");
            table.boolean("Iodine");
            table.boolean("Iron");
            table.boolean("Magnesium");
            table.boolean("Manganese");
            table.boolean("Molybdenum");
            table.boolean("Nickel");
            table.boolean("Phosphorus");
            table.boolean("Potassium");
            table.boolean("Selenium");
            table.boolean("Sodium");
            table.boolean("Vanadium");
            table.boolean("Zinc");
            
        });
    }
};


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("foodnull");
}
