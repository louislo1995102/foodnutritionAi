import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(!await knex.schema.hasTable("whatyoueatb4")){
        await knex.schema.createTable("whatyoueatb4",(table)=>{
            table.increments();
            table.integer("user_id");
            table.string("food");
            table.string("nutritionDeficiency");
            table.timestamp("createRecord");
        });
    }
};


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("whatyoueatb4");
}
