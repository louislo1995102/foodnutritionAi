import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(!await knex.schema.hasTable("foodnutrition")){
        await knex.schema.createTable("foodnutrition",(table)=>{
            table.increments();
            table.string('food');
            table.boolean("Protein");
            table.boolean("Biotin");
            table.boolean("Folate");
            table.boolean("VitaminA");
            table.boolean("VitaminB1");
            table.boolean("VitaminB2");
            table.boolean("VitaminB3");
            table.boolean("VitaminB6");
            table.boolean("VitaminB12"); 
            table.boolean("VitaminC");
            table.boolean("VitaminD");
            table.boolean("VitaminE");
            table.boolean("VitaminK");
            table.boolean("Boron");
            table.boolean("Calcium");
            table.boolean("Chlorine");
            table.boolean("Chromium");
            table.boolean("Copper");
            table.boolean("Fluorine");
            table.boolean("Iodine");
            table.boolean("Iron");
            table.boolean("Magnesium");
            table.boolean("Manganese");
            table.boolean("Molybdenum");
            table.boolean("Nickel");
            table.boolean("Phosphorus");
            table.boolean("Potassium");
            table.boolean("Selenium");
            table.boolean("Sodium");
            table.boolean("Vanadium");
            table.boolean("Zinc");
            
        });
    }
};


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("foodnull");
}