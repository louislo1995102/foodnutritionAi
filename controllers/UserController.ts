// request. response related
import { UserService } from "../services/UserService";
import { Request, Response } from "express";
import { checkPassword } from "../hash";

export class UserController {
    constructor(private userService: UserService) {}

    login = async (req: Request, res:Response) => {
        try{
            const username = req.body.username;
            const password = req.body.password;

            console.log(password)
            if(!username || !password) {
                res.status(400).json({message:"invalid input!"})
                return;
            }
            const user = await this.userService.getUsers(username);

            console.log(user)

            if(!user || !await(checkPassword(password,user.password) ) ){
                res.status(400).json({message:"incorrect password/username"});
                return;
            }
            req.session['user'] = user.id

            res.status(200).json({success:true,msg:'success'})
        }
        catch(err){
            console.log(err.message);
            res.status(500)
        }
    }

    createUser = async(req:Request,res:Response)=>{
            const {username, password, email, gender, date_of_birth, phone_number} = req.body;
            console.log(req.body)
            await this.userService.createUser(username, password, email, gender, date_of_birth, phone_number);
            res.redirect('/login.html');
        }
    
    
    logout = async(req:Request,res:Response)=>{
            delete req.session['user'];
            res.redirect('/login.html')
        }

}