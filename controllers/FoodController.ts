import { Request, Response } from "express";
import { FoodService } from "../services/FoodService";
import { Food } from "../services/models";
import fs from "fs";
import fetch from "node-fetch";
//import { Knex } from "knex";
//import express from 'express';
//import expressSession from 'express-session';

// No DB related , only on express
export class FoodController {
  constructor(private foodService: FoodService) {}

  insertFood = async (req: Request, res: Response) => {
    // const name = req.body.content;
    // await this.foodService.createFood(name);
    // res.json({ success: true });

    try {
      const data = req.body;
      console.log(data);
      data["image64"];
      let base64String = data["image64"];
      let base64Image = base64String.split(";base64,").pop();
      let a = Math.random();
      let str = String(a) + ".jpeg";
      
      console.log(req.session['user'])

      fs.writeFile(
        "./AiServer/" + str,
        base64Image,
        { encoding: "base64" },
        function (err) {
          console.log("File created");
        }
      );
      
      const res1 = await fetch("http://localhost:8000/prediction?name=" + str);
      // this is the prediction result :(res.json)

      const foodObject = await res1.json();
      const foodStr:string =JSON.stringify(Object.values(foodObject)[0]);
      const food:string = foodStr.slice(1,-1);
      
      console.log(food);

      const createRecord = new Date();

      //const user = await knex.select('username').from('users').where.where('id', '');
        
      const user_id = req.session['user'];

      await this.foodService.createFood(user_id,food, createRecord);

      res.json({ message: "internal server ok" });
        
   
    } catch (e) {
      console.log(e);

      res.json({ message: "internal server error" });
    }
  };

  getFood = async (req: Request, res: Response) => {

    let id = req.session['user'];

    const food:Food[]=await this.foodService.showFood(id);
    console.log(food)

    res.json(food);
    // const queryString = req.query.q + "";
    // if (req.query.q && queryString) {
    //   // 先寫好SQL，就識寫呢度
    //   const food: Food[] = await this.foodService.showFood(queryString);
    //   res.json(food);
    //   console.log(res.json(food));
    // } else {
    //   const result = await this.foodService.showFood();
    //   let food: Food[] = result;
    //   res.json(food);
    // }
  };

  eatHistory = async (req: Request, res: Response) => {

    let id = req.session['user'];

    const food:Food[]=await this.foodService.showDate(id);
    console.log(food)

    res.json(food);
    // const queryString = req.query.q + "";
    // if (req.query.q && queryString) {
    //   // 先寫好SQL，就識寫呢度
    //   const food: Food[] = await this.foodService.showFood(queryString);
    //   res.json(food);
    //   console.log(res.json(food));
    // } else {
    //   const result = await this.foodService.showFood();
    //   let food: Food[] = result;
    //   res.json(food);
    // }
  };
  // updateFood = async(req:Request,res:Response)=>{
  //     const id = parseInt(req.params.id);
  //     if(isNaN(id)){
  //         res.status(400).json({msg:"id is not an integer"});
  //         return;
  //     }
  //     const content = req.body.content;

  //     this.foodService.updateMemo(id,content);
  //     res.json({success:true});
  // }

  // deleteFood = async(req:Request,res:Response) =>{
  //     const id = parseInt(req.params.id);
  //     if(isNaN(id)){
  //         res.status(400).json({msg:"id is not an integer"});
  //         return;
  //     }
  //     this.foodService.deleteMemo(id);
  //     res.json({success:true});
  // }
}
