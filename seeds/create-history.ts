import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("history").del();

    // Inserts seed entries
    await knex("history").insert([
        {
            user_id:3,
            food:"fuknirice",
            createRecord:'2021-09-09 07:31:21'

        },
        {
            user_id:3,
            food:"fuknirice",
            createRecord:'2021-09-09 12:04:41'

        }   ,
        {
            user_id:2,
            food:"SingaporeStir-friedNoodles",
            createRecord:'2021-09-09 23:48:51'

        },
        {
            user_id:2,
            food:"SatayBeefNoodle",
            createRecord:'2021-09-09 07:30:51'

        },
        {
            user_id:1,
            food:"Stir-fried-BeefNoodle",
            createRecord:'2021-09-09 07:30:51'

        }
    ]);
};
