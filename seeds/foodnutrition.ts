import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("foodnutrition").del();

    // Inserts seed entries
    await knex("foodnutrition").insert([
        {food: "Stir-fried-BeefNoodle",
        Protein:true,
        Biotin:false,
        Folate:false,
        VitaminA:false,
        VitaminB1:false,
        VitaminB2:false,
        VitaminB3:false,
        VitaminB6:true,
        VitaminB12:true,
        VitaminC:false,
        VitaminD:false,
        VitaminE:false,
        VitaminK:false,
        Boron:false,
        Calcium:false,
        Chlorine:false,
        Chromium:false,
        Copper:false,
        Fluorine:false,
        Iodine:false,
        Iron:true,
        Magnesium:true,
        Manganese:false,
        Molybdenum:false,
        Nickel:false,
        Phosphorus:true,
        Potassium:true,
        Selenium:true,
        Sodium:false,
        Vanadium:false,
        Zinc:true,
    },
    {food:"SingaporeStir-friedNoodles",
        Protein:true,
        Biotin:false,
        Folate:false,
        VitaminA:true,
        VitaminB1:false,
        VitaminB2:false,
        VitaminB3:false,
        VitaminB6:false,
        VitaminB12:false,
        VitaminC:true,
        VitaminD:false,
        VitaminE:false,
        VitaminK:false,
        Boron:false,
        Calcium:false,
        Chlorine:false,
        Chromium:false,
        Copper:false,
        Fluorine:false,
        Iodine:false,
        Iron:false,
        Magnesium:false,
        Manganese:false,
        Molybdenum:false,
        Nickel:false,
        Phosphorus:false,
        Potassium:true,
        Selenium:false,
        Sodium:false,
        Vanadium:false,
        Zinc:false,
    },
    {food:"fuknirice",
        Protein:true,
        Biotin:false,
        Folate:false,
        VitaminA:true,
        VitaminB1:false,
        VitaminB2:true,
        VitaminB3:false,
        VitaminB6:false,
        VitaminB12:true,
        VitaminC:false,
        VitaminD:false,
        VitaminE:false,
        VitaminK:false,
        Boron:false,
        Calcium:true,
        Chlorine:false,
        Chromium:false,
        Copper:false,
        Fluorine:false,
        Iodine:false,
        Iron:true,
        Magnesium:true,
        Manganese:false,
        Molybdenum:false,
        Nickel:false,
        Phosphorus:true,
        Potassium:true,
        Selenium:true,
        Sodium:false,
        Vanadium:false,
        Zinc:true,
    },
    {food:"SatayBeefNoodle",
        Protein:true,
        Biotin:false,
        Folate:true,
        VitaminA:true,
        VitaminB1:false,
        VitaminB2:false,
        VitaminB3:false,
        VitaminB6:true,
        VitaminB12:true,
        VitaminC:true,
        VitaminD:true,
        VitaminE:true,
        VitaminK:false,
        Boron:false,
        Calcium:true,
        Chlorine:false,
        Chromium:false,
        Copper:false,
        Fluorine:false,
        Iodine:false,
        Iron:true,
        Magnesium:true,
        Manganese:true,
        Molybdenum:false,
        Nickel:false,
        Phosphorus:true,
        Potassium:true,
        Selenium:true,
        Sodium:false,
        Vanadium:false,
        Zinc:true,
    }
    ]);
};