import { Knex } from "knex";
import { hashPassword } from "../hash"; 
export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("users").del();

    // Inserts seed entries
    const hash = await hashPassword('1234')
    await knex("users").insert([
        {username: "alvin ma", email: "alvin672335@gmail.com",password: hash,gender:"Male",date_of_birth:"1997-09-22",phone_number:"12345678"},
        {username: "john pang", email: "johnpang@tecky.io",password: hash,gender:"Male",date_of_birth:"2003-10-20",phone_number:"12345678"},
        {username: "louis lo", email: "louislo@tecky.io",password: hash,gender:"Male",date_of_birth:"2003-10-02",phone_number:"12345678"}
    ]);
};
